**Social and Open Technology Community - Work Report Nov 2023 to 2024**

SOTC NOV-2023

    > [SOTC] - 1st Social &Open Technologies community Conference - 10/Nov/2023

    > [Aishwarya] - Designing with Inkscape - 21/Nov/2023
        - Topics: Vector image and Rastor image, 

    > [Aishwarya] - Inkscape Hands-on - 28/Nov/2023
        - Topics: Basic tools includes Pen tool, gradient tool, font importing, Tracing Bitmap and Poster creation.
    
SOTC DEC-2023

    > [Gurupriyan] - Python [Part - 1] - 5/Dec/2023
        - Topics: Introduction to Data structures, variables, methods.
        Building Hangman game.

    > [Gurupriyan] - Python [Part - 2] - 10/Dec/2023
        - Topics:

    > [SOTC] - Delicates to FSHM's 2nd Organisation Conference - 17/Dec/2023  
    
    > [Aanisha & Gurupriyan] - Python [Part -3] - 19/Dec/2023
        -Topics: Introduction to oop - objects, properties, defining a class, instances, built-in __init__() function. 
    
SOTC MAR-2024

    > [Aanisha] - DBMS - 12/Mar/2024
        - Topics: Introduction to SQL, DDL, DML and implementation of queries.

    > [Sridhar] - Build your Portfolio Website [Part - 1] - 19/Mar/2024
        - Topics: Why portfolio?, Introduction to markdown, Static site generator(Pelican and Jekyll), Explanation on SOTC Blogs.

SOTC APR-2024    

    > [Sridhar] - Build your Portfolio Website [Part - 2] - 4/Apr/2024
        -Topics: Understanding Hugo's file structure, modifying kross-hugo themes, hosting gitlab pages.  



