## EC Members

#### Executive Committee 2023 - present

Sl.No. | Name | Position | Email
---|---|---|---
1 | Aanisha Almaaz S | President | aanishaalmaaz03@proton.me
2 | Sridhar | Secretary | sridhar79.rgss@proton.me
3 | Parameswari | Treasurer | parameswarik94@gmail.com
4 | Johan | Vice President | johanthomasrobert@gmail.com
5 | Aravindan | Joint Secretary | aravindansharma312@gmail.com
6 | Aishwarya | EC member | aishwaryaraman838@gmail.com
7 | Akash | EC member | akashsree50@gmail.com
8 | Dinaesh | EC member | dinaeshdinu2002@gmail.com
9 | Ghanesh | EC member | ghaneshmouthouvel@gmail.com
10 | Gurupriyan |  EC member | am400718@gmail.com
11 | Kiruthika M | EC member | kiruthikam1712@gmail.com


#### Executive Committee 2020 - 2023

Sl.No. | Name | Email
---|---|---
1 | Arunekumar | 
2 | Arjun | arjunsv0607@gmail.com
3 | Chandru Kumar | schandrukumarsck@gmail.com
4 | Dinaesh | dinaeshdinu2002@gmail.com
5 | Ghanesh | ghaneshmouthouvel@gmail.com
6 | Jayanth | jayanthravindar@gmail.com
7 | Ken Harris | yo@kenharris.xyz
8 | Kiruthiga | kiruanime2003@gmail.com
9 | Noordine | noordine@tutanota.com
10 | Parameswari | parameswarik94@gmail.com
11 | Parthipan | parthipank51@gmail.com
12 | Roshan Rihana | roshrihana25@gmail.com
13 | Shibi Nandan | shibinandanr@gmail.com
14 | Sridhar | sridhar79.rgss@gmail.com
