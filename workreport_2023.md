﻿**Social and Open Technology Community - Work Report 2023**

SOTC 04-2023

    > [SOTC] - Delicates to Chennai FOSS Conference - 24/Apr/2023

SOTC 06-2023

    > [Ghanesh] - Django - A python web framework [Part - 1] - 18/Jun/2023
        - Topics: Hands-on CRUD Operations
    > [SOTC] - Hacktivist Camp - 26/Jun/2023
        - Topics:
             + Session 1 - Sex, Gender, Sexuality
             + Session 2 - GNU/Linux ecosystem
             + Session 3 - Intellectual Property Rights
             + Session 4 - Caste and Religion
             + Session 5 - Prespective of Society and technology
             + Session 6 - Platform Co-operativism
             + Session 7 - Self Hosting
             + Session 8 - Cloud Computing and DevOps
             + Session 9 - Organization

SOTC 07-2023

    > [Ghanesh] - Django - A python web framework [Part - 2] - 02/Jul/2023
        - Topics: Custom Auth User Model, Authendication and Authendicate
    > [Ragulkanth] - Self Hosting - 08/Jul/2023
        - Topics: Understanding cloud, Nextcloud, pi-hole, jellyfin, bitwarden
    > [Kamal] - Docker [Part - 1] - 16/Jul/2023
        - Topics: Understanding Containers, Docker Images and Docker Containers
    > [Ghanesh, Kamal] - Docker [Part - 2] - 27/Jul/2023
        - Topics: Introduction to FOSS, Licencing, Web server, Nginx, Firewall
    > [Kamal] - Docker [Part - 3] - 30/Jul/2023
        - Topics: HTTPS, TLS, Let's Encrypt, Docker Network, Port forwarding

SOTC 08-2023

    > [Kamal] - Docker [Part - 4] - 13/Aug/2023
        - Topics: Docker performance, Security and build a docker container
    >[SOTC] - Discussion on FOSS - 20/Aug/2023
        - Topics: A wider discuessing on FOSS
        - Celebrated 30Yrs for Debian
    > [Dinaesh, Arjun] - Git and Gitlab - 27/Aug/2023
        - Topics: What is version control system, Why git, what is SSH, git {add || commit || push || pull || revoke || merge || branch}

SOTC 09-2023

    > [Ranjith] - Fundamentals of electronics - 03/Sept/2023
        - Topics: Hardware freedom, Concepts of voltage, current, resistance, stepup & stepdown trasnformer
    > [Kamal, SOTC] - DevOps & Cloud Computing - 08/Sept/2023
        - Topics: Understanding Cloud, SRE, DevOps, Docker, Kubernatives, Terraform
    > [SOTC] - Software freedom day by FSHM - 24/Sept/2023
        - Stalls by SOTC:
            + [Dinaesh, Johan] - Android, Custom ROMs and alternatives
            + [Ghanesh, Rihana, Sakthi] - GIS and Open Data
            + [Vignesh, Kiruthika] - Climate Change
            + [Sridhar, Aanisha] - Weather Station and Citizen Science
            + [Reshma, Paramaeshwari, Agila] - Health & Gender
            + [Arjun, Aishwariya] - Design & Multimeida in FOSS
            + [Nivadha, Mugindhar] - Open Wiki and Knowledge Commons
    > [Jagadeesh] - Data Structure & Algorithm [Part - 1] - 29/Sept/2023
        - Topics: Sorting Algorithms, Hash Map & its implementation

SOTC 10-2023

    > [Jagadeesh] - Data Structure & Algorithm [Part - 2] - 08/Oct/2023
        - Topics: Stack, Queue, Double ended Queue, Linked List & its implementation
    > [Jagadeesh] - Data Structure & Algorithm [Part - 3] - 15/Oct/2023
        - Topics: Traversal Technique in Binary tree & its implementation
    > [Jagadeesh] - Data Structure & Algorithm [Part - 4] - 22/Oct/2023
        - Topics: Solving Leetcode Problems
    > [Dinaesh] - Free Your Droid - 29/Oct/2023
        - Topics: Introduction to FOSS, Bootloader, Rooting, ADB, Data Privacy, MicroG, SafetyNet
