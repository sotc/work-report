﻿**Social and Open Technology Community - Work Report 2021**

Created on 10/Jan/2021

SOTC, 01-2021

    > [ Prasanna Venkadesh ] - Why Signal ? - 12/Jan/2021 #OnlineMeetup
        - Topics: [ How Technology Discriminates ?, Role of Facebook and Whatsapp in The Democracy, What are The Alternatives ? ]

SOTC, 04-2021

    > Created Peertube Account on 07/Apr/2021
    > [ Ken Harris ] - Python and Web Development - 15/Apr/2021
        - Topics: Introduction Session
    > [ Ken Harris ] - Python and Web Development - 22/Apr/2021
        - Project: Calculator
        - Code: https://gitlab.com/sotc/PythonWebDev/-/tree/master/Calculator
    > [ Ken Harris ] - Python and Web Development - 27/Apr/2021
        - project: Currency Convertor
        - Code: https://gitlab.com/sotc/PythonWebDev/-/tree/master/Currency-Converter
    
SOTC, 05-2021

    > [ Kathirvel ] - Networking and Security - 02/May/2021
        - Topics: Introduction Session
    > [ Ken Harris ] - Python and Web Development - 04/May/2021
        - Project: Rock-Paper-Scissors (Part 1)
    > [ Ken Harris ] - Python and Web Development - 08/May/2021
        - Project: Rock-Paper-Scissors (Part 2), Dice Program
        - Code: https://gitlab.com/sotc/PythonWebDev/-/tree/master/Rock-Paper-Scissors
    > [ Ken Harris ] - Python and Web Development - 12/May/2021
        - Project: PDF Compiler (Part 1)
    > [ Ken Harris ] - Python and Web Development - 13/May/2021
        - Project: PDF Compiler (Part 2)
        - Code: https://gitlab.com/sotc/PythonWebDev/-/tree/master/PDF-Compiler
    > [ Kathirvel ] - Networking and Security - 17/May/2021
        - Topics: Why & How computer communicates, Types & Ways of communication and Topologies.
    > [ Ken Harris ] - Python and Web Development - 18/May/2021
        - Project: Birthday Logger (Part 1)
    > [ Ken Harris ] - Python and Web Development - 21/May/2021
        - Project: Birthday Logger (Part 2)
        - Code: https://gitlab.com/sotc/PythonWebDev/-/tree/master/Birthday-Logger
    > [ Ken Harris ] - Python and Web Development - 25/May/2021
        - Topic: "India Information Technology rules 2021"
    > [ Ken Harris ] - Python and Web Development - 29/May/2021
        - Project: Movie Organizer (Part 1)

SOTC, 06-2021

    > [ Ken Harris ] - Python and Web Development - 01/Jun/2021
        - Project: Movie Organizer (Part 2)
    > [ Ken Harris ] - Python and Web Development - 15/Jun/2021
        - Topic: Concept of OOPs

SOTC, 11-2021

    > [ Dhamodharan ] - Java and Web Development - 27/Nov/2021
        - Topic: Introductory class

SOTC, 12-2021

    > [ Saif Ahmed, Banisha warjri ] - Java and Web Development - 09/Dec/2021
        - Topic: Basic data types, Operators and Operands
    > [ Saif Ahmed, Banisha warjri ] - Java and Web Development - 10/Dec/2021
        - Topic: if statements, Loops
    > [ Saif Ahmed, Banisha warjri ] - Java and Web Development - 17/Dec/2021
        - Topic: Loops
