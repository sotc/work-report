﻿**Social and Open Technology Community - Work Report 2022**


SOTC, 01-2022

    > [ Saif Ahmed ] - Java and Web Development - 04/Jan/2022
        - topic: Java Methods, Method Parameters
    > [ Saif Ahmed, Banisha warjri ] - Java and Web Development - 07/Jan/2022
        - Topic: Method Overriding, Scopes
    > [ Dinaesh, Ken Harris ] - Free Software and Open Source Software(FL/OSS) - 12/01/2022
        - Topic: Recursion
    > [ Saif Ahmed, Banisha warjri ] - Java and Web Development - 13/Jan/2022
        - Topic: Arrays

SOTC, 02-2022

    > [ Saif Ahmed, Banisha warjri ] - Java and Web Development - 04/Feb/2022
        - topic: Arrays
    > [ Saif Ahmed, Banisha warjri ] - Java and Web Development - 11/Feb/2022
        - topic: Functions and Object Oriented Programming

SOTC, 09-2022

    > [SOTC]- Game-Jam - 21/Sept/2022 #Competition
        - topic: Game Development using FOSS tools
        - Participants: 80+
        - Prize: Joystick for First & Second Position

SOTC, 10-2022

    > [SOTC] - Hacktober Fest 2022 #Participation
        - 15 Participation towards FOSS in PU
